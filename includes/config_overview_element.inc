<?php


abstract class config_overview_element  {


    function __construct($module='', $type='', $id='') {

        $this->module = $module;
        $this->type = $type;
        $this->id = $id;

        // path to form where this doku is updated
        $this->path = '';

        if ($module && $type && $id) {
            $this->loadByParams($module, $type, $id);
        }
    }
function getFormIds () {
  return array();
}

  function setId ($id) {
    $this->id = $id;
  }

  private function loadByParams($module, $type, $id, $lid='da') {
        $this->module = $module;
        $this->type = $type;
        $this->id = $id;

        $this->load();

    }

    public function load() {
        $this->loadTitle();
        $infofile = drupal_parse_info_file(        drupal_get_path('module', $this->module) . '/' . $this->module . '.info');
        $this->moduleTitle = $infofile['name'];

         // invoke hook config_overview_element_load
        foreach (module_implements('config_overview_element_load') as $module) {
            $function = $module . '_config_overview_element_load';
            if (function_exists($function))
                $function($this);
        }
    }



    function getModule() { return $this->module; }
    function getModuleTitle() { return $this->moduleTitle; }
    function getType() { return $this->type; }
    function getId() { return $this->id; }

     function getAction () {
       return 'edit';
     }

    function loadByTitle($title) {
    }

    public function applyValues(stdClass $object) {
        foreach (get_object_vars($object) as $k => $v)
            $this->$k = $v;
    }

    function getTypetitle() {
        if (isset($this->typetitle) && $this->typetitle)
            return $this->typetitle;

        return 'Element';
    }

    function hasTitle() {
        return (bool) trim($this->title);
    }

    function getTitle() {
        if (isset($this->title) && $this->title)
            return $this->title;
        $this->loadTitle();
        if (isset($this->title) && $this->title)
           return $this->title;

        return $this->getKey();
    }

    function getFullTitle() {
        return $this->getTypetitle() . ' "' . $this->getTitle() . '"';
    }

    function setTitle($title) {
        if (trim($title))
            $this->title = $title;
    }

    function getTitleFormatted() {
        // if we have a title, output title and key
        if (isset($this->title) && $this->title)
            return '<div class="doku-title">' . $this->title . '</div>' .
            '<div class="doku-key">' . $this->getKey() . '</div>';

        // no title - output key as title
        return
            '<div class="doku-title">' . $this->getKey() . '</div>';


    }

    function get_edit_form_ids () {

    }

    function getKey($anchor=FALSE) {
        if ($anchor)
            // for use in anchors
        return sprintf('%s-%s-%s', $this->module, $this->type, $this->id);

        // for use by humans
        return sprintf('%s/%s/%s', $this->module, $this->type, $this->id);
    }

    function getViewHref($full=FALSE, $abs=FALSE) {
        $href = '#' . $this->getKey(1);
        if ($full) {
            $href = '/admin/reports/doku/report' . $href;

            if ($abs)
                $href = $_SERVER['REQUEST_HOST'] . $href;

        }
        return $href;

    }

    function getLinks() {
        $links = module_invoke_all('doku_links', $this);
        foreach ($links as $i => $link) {
            $links[$i] += array(
                '#options' => array(
                    'attributes' => array(),
                ),
            );
        }
        return theme_links(array(
            'links' => $links,
            'attributes' => array(),
            'heading' => '',
        ));
    }

    function getEditHref() {
        return $this->path;
    }


    public function getBodyCropped ($l=30) {
        return substr(strip_tags($this->body), 0, $l);
    }

    abstract function loadTitle();
    abstract public function loadFromForm ($form, $form_state, $form_id);



}

/*
 *
 class config_overview_element_service {

    static public function getInstance($module, $type, $id=NULL){

    }

    static public function getInstanceByKey ($key) {
        $parts = explode('/', $key);
        $module = array_shift($parts);
        $type = array_shift($parts);
        $id = sizeof($parts) ? implode('/', $parts) : NULL;

        return self::getInstance($module, $type, $id);
    }
}
*/