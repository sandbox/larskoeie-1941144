<?php


  /*
   *
   *
   *
   */

  class config_overview_element_system_module extends config_overview_element {

    function __construct(){
      parent::__construct('block', 'block');
      $this->typetitle = 'Block';

    }

    function setId ($id) {
      parent::setId($id);
      $parts = explode('/', $id);
      if (sizeof($parts)) {
        $this->block_module = $parts[0];
        $this->block_delta = $parts[1];
      }
      $this->loadTitle();
    }


    function loadTitle() {
      if (isset($this->block_module) && isset($this->block_delta)) {

        $block = block_load($this->block_module, $this->block_delta);
        PRINT_R($block);

        if (isset($block->title))
          $this->title = htmlentities($block->title);
      }
    }

    function getFormIds () {
      return array(
        'system_modules' => '',
      );
    }

    function loadFromForm ($form, $form_state, $form_id) {
      $this->id = 'overview';
      if (isset($form['module']) && isset($form['delta']))
        $this->setId($form['module']['#value'] . '/' . $form['delta']['#value']);

    }
  }